var express = require("express");
var knex = require("knex");
var ee = require("events");

var dbConfig = require("./knexfile");
var app = express();

var port = 3000;

var statEmitter = new ee();
var stats = {
  totalUsers: 3,
  totalBets: 1,
  totalEvents: 1,
};

var db;
app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => {
  db = knex(dbConfig.development);
  db.raw("select 1+1 as result")
    .then(function () {
      neededNext();
    })
    .catch(() => {
      throw new Error("No db connection");
    });
});

const routes = require("./src/api/routes/index");
routes(app);

app.get("/health", (req, res) => {
  res.send("Hello World!");
});

app.listen(port, () => {
  statEmitter.on("newUser", () => {
    stats.totalUsers++;
  });
  statEmitter.on("newBet", () => {
    stats.totalBets++;
  });
  statEmitter.on("newEvent", () => {
    stats.totalEvents++;
  });

  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };
