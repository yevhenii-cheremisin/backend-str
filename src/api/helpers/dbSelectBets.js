function validData(variable) {
  let array = [
    "bet_amount",
    "event_id",
    "away_team",
    "home_team",
    "odds_id",
    "start_at",
    "updated_at",
    "created_at",
    "user_id",
  ];
  let newVar = forEachParam(array, variable);
  return newVar;
}

function forEachParam(arr, forWhat) {
  arr.forEach((whatakey) => {
    var index = whatakey.indexOf("_");
    var newKey = whatakey.replace("_", "");
    newKey = newKey.split("");
    newKey[index] = newKey[index].toUpperCase();
    newKey = newKey.join("");
    forWhat[newKey] = forWhat[whatakey];
    delete forWhat[whatakey];
  });
  return forWhat;
}

function dbSelectBets(req, res, db, statEmitter) {
  db("odds")
    .where("id", event.odds_id)
    .then(([odds]) => {
      if (!odds) {
        return res.status(404).send({ error: "Odds not found" });
      }
      let multiplier;
      switch (req.body.prediction) {
        case "w1":
          multiplier = odds.home_win;
          break;
        case "w2":
          multiplier = odds.away_win;
          break;
        case "x":
          multiplier = odds.draw;
          break;
      }
      db("bet")
        .insert({
          ...req.body,
          multiplier,
          event_id: event.id,
        })
        .returning("*")
        .then(([bet]) => {
          var currentBalance = user.balance - req.body.bet_amount;
          db("user")
            .where("id", userId)
            .update({
              balance: currentBalance,
            })
            .then(() => {
              statEmitter.emit("newBet");
              bet = validData(bet);
              return res.send({
                ...bet,
                currentBalance: currentBalance,
              });
            });
        });
    });
}

module.exports = {
  dbSelectBets,
};
