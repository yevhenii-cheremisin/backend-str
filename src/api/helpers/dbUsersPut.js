function dbUsersPut(req, res, db) {
  db("user")
    .where("id", req.params.id)
    .update(req.body)
    .returning("*")
    .then(([result]) => {
      return res.send({
        ...result,
      });
    })
    .catch((err) => {
      if (err.code == "23505") {
        console.log(err);
        res.status(400).send({
          error: err.detail,
        });
        return;
      }
      console.log(err);
      res.status(500).send("Internal Server Error");
      return;
    });
}

module.exports = {
  dbUsersPut,
};
