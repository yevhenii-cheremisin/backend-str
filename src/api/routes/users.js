const { dbUsersPut } = require("../helpers/dbUsersPut");

const { Router } = require("express");

const router = Router();

var jwt = require("jsonwebtoken");
var joi = require("joi");
var knex = require("knex");

var dbConfig = require("../../../knexfile");
var db;

var ee = require("events");
var statEmitter = new ee();

db = knex(dbConfig.development);
db.raw("select 1+1 as result")
  .then(function () {
    neededNext();
  })
  .catch(() => {
    throw new Error("No db connection");
  });

function schemaCreateGet() {
  let schema = joi
    .object({
      id: joi.string().uuid(),
    })
    .required();
  return schema;
}

function schemaCreatePut() {
  let schema = joi
    .object({
      email: joi.string().email(),
      phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
      name: joi.string(),
      city: joi.string(),
    })
    .required();
  return schema;
}

function schemaCreatePost() {
  let schema = joi
    .object({
      id: joi.string().uuid(),
      type: joi.string().required(),
      email: joi.string().email().required(),
      phone: joi
        .string()
        .pattern(/^\+?3?8?(0\d{9})$/)
        .required(),
      name: joi.string().required(),
      city: joi.string(),
    })
    .required();
  return schema;
}

router.get("/:id", (req, res) => {
  try {
    var schema = schemaCreateGet();
    var isValidResult = schema.validate(req.params);
    if (isValidResult.error) {
      res.status(400).send({ error: isValidResult.error.details[0].message });
      return;
    }
    db("user")
      .where("id", req.params.id)
      .returning("*")
      .then(([result]) => {
        if (!result) {
          res.status(404).send({ error: "User not found" });
          return;
        }
        return res.send({
          ...result,
        });
      });
  } catch (err) {
    console.log(err);
    res.status(500).send("Internal Server Error");
    return;
  }
});

router.post("", (req, res) => {
  var schema = schemaCreatePost();
  var isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  }
  req.body.balance = 0;
  //dbUsersPost.js
  db("user")
    .insert(req.body)
    .returning("*")
    .then(([result]) => {
      result.createdAt = result.created_at;
      delete result.created_at;
      result.updatedAt = result.updated_at;
      delete result.updated_at;
      statEmitter.emit("newUser");
      return res.send({
        ...result,
        accessToken: jwt.sign(
          { id: result.id, type: result.type },
          process.env.JWT_SECRET
        ),
      });
    })
    .catch((err) => {
      if (err.code == "23505") {
        res.status(400).send({
          error: err.detail,
        });
        return;
      }
      res.status(500).send("Internal Server Error");
      return;
    });
});

router.put("/:id", (req, res) => {
  let token = req.headers["authorization"];
  let tokenPayload;
  if (!token) {
    return res.status(401).send({ error: "Not Authorized" });
  }
  token = token.replace("Bearer ", "");
  try {
    tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
  } catch (err) {
    return res.status(401).send({ error: "Not Authorized" });
  }
  let schema = schemaCreatePut();
  var isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  }
  if (req.params.id !== tokenPayload.id) {
    return res.status(401).send({ error: "UserId mismatch" });
  }
  dbUsersPut(req, res, db);
});

module.exports = router;
