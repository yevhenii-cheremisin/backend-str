const { dbUsers } = require("../helpers/dbUsers");

const { Router } = require("express");

const router = Router();

var jwt = require("jsonwebtoken");
var joi = require("joi");
var knex = require("knex");

var dbConfig = require("../../../knexfile");
var db;

db = knex(dbConfig.development);
db.raw("select 1+1 as result")
  .then(function () {
    neededNext();
  })
  .catch(() => {
    throw new Error("No db connection");
  });

router.post("/", (req, res) => {
  var schema = joi
    .object({
      id: joi.string().uuid(),
      userId: joi.string().uuid().required(),
      cardNumber: joi.string().required(),
      amount: joi.number().min(0).required(),
    })
    .required();
  var isValidResult = schema.validate(req.body);
  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  }

  let token = req.headers["authorization"];
  if (!token) {
    return res.status(401).send({ error: "Not Authorized" });
  }
  token = token.replace("Bearer ", "");
  try {
    var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    if (tokenPayload.type != "admin") {
      throw new Error();
    }
  } catch (err) {
    return res.status(401).send({ error: "Not Authorized" });
  }

  dbUsers(req, res, db);
});

module.exports = router;
