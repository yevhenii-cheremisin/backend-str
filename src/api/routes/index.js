const betsRoute = require("./bets");
const eventsRoutes = require("./events");
const statsRoutes = require("./stats");
const transactionsRoutes = require("./transactions");
const usersRoutes = require("./users");

module.exports = (app) => {
  app.use("/users", usersRoutes);
  app.use("/transactions", transactionsRoutes);

  app.use("/bets", betsRoute);
  app.use("/events", eventsRoutes);
  app.use("/stats", statsRoutes);
};
