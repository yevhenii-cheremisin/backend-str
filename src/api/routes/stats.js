const { Router } = require("express");

const router = Router();

var dbConfig = require("../../../knexfile");
var db;

var jwt = require("jsonwebtoken");
var knex = require("knex");
var stats = {
  totalUsers: 4,
  totalBets: 2,
  totalEvents: 2,
};

db = knex(dbConfig.development);
db.raw("select 1+1 as result")
  .then(function () {
    neededNext();
  })
  .catch(() => {
    throw new Error("No db connection");
  });

router.get("", (req, res) => {
  try {
    let token = req.headers["authorization"];
    if (!token) {
      return res.status(401).send({ error: "Not Authorized" });
    }
    token = token.replace("Bearer ", "");
    try {
      var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
      if (tokenPayload.type != "admin") {
        throw new Error();
      }
    } catch (err) {
      return res.status(401).send({ error: "Not Authorized" });
    }
    console.log(stats);
    res.send(stats);
  } catch (err) {
    console.log(err);
    res.status(500).send("Internal Server Error");
    return;
  }
});

module.exports = router;
